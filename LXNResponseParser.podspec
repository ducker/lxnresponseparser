Pod::Spec.new do |s|
  s.name                  = 'LXNResponseParser'
  s.version               = '1.0'
  s.summary               = ''
  s.author                = { 'Leszek Kaczor' => 'leszekducker@gmail.com' }
  s.source                = { :git => 'https://bitbucket.org/ducker/responseparser.git' }
  s.source_files          = 'ResponseParser/LXNResponseParser/*'
end
