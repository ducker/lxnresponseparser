//
//  ResponseParserTests.m
//  ResponseParserTests
//
//  Created by Leszek Kaczor on 04.04.2014.
//  Copyright (c) 2014 Untitled Kingdom. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "LXNResponseParser.h"
#import "LXNTopicROM.h"
#import "LXNTopicCapitalizedROM.h"
#import "LXNTopicTypoROM.h"
#import "LXNTopicAdditionalPropertiesROM.h"

@interface ResponseParserTests : XCTestCase

@end

@implementation ResponseParserTests

+ (void)setUp
{
    [LXNResponseParser logTime:YES];
    NSDateFormatter * formatter = [NSDateFormatter new];
    [formatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss'Z'"];
    [LXNResponseParser dateFormatter:formatter];
}

- (void)setUp
{
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown
{
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)testSingleObject
{
    NSString *filePath = [[NSBundle mainBundle] pathForResource:@"SingleTopic" ofType:@"json"];
    NSString * jsonString = [[NSString alloc] initWithContentsOfFile:filePath encoding:NSUTF8StringEncoding error:nil];
    
    NSError * e;
    
    id response = [NSJSONSerialization JSONObjectWithData:[jsonString dataUsingEncoding:NSUTF8StringEncoding] options:kNilOptions error:&e];
    
    id responseObject = [LXNResponseParser parseResponse:response usingClass:[LXNTopicROM class]];
    
    XCTAssertTrue([responseObject isKindOfClass:[LXNTopicROM class]], @"Object is not kind of class %@ - %@ \"%s\"", [LXNTopicROM class], [responseObject class] ,__PRETTY_FUNCTION__);
}

- (void)testSingleObjectWithCapitalizedProperties
{
    NSString *filePath = [[NSBundle mainBundle] pathForResource:@"SingleTopic" ofType:@"json"];
    NSString * jsonString = [[NSString alloc] initWithContentsOfFile:filePath encoding:NSUTF8StringEncoding error:nil];
    
    NSError * e;
    
    id response = [NSJSONSerialization JSONObjectWithData:[jsonString dataUsingEncoding:NSUTF8StringEncoding] options:kNilOptions error:&e];
    
    id responseObject = [LXNResponseParser parseResponse:response usingClass:[LXNTopicROM class]];
    id capitalizedObject  = [LXNResponseParser parseResponse:response usingClass:[LXNTopicCapitalizedROM class]];
    
    NSSet * responseProperties = [LXNTopicROM getProperties];
    NSSet * capitalizedProperties = [LXNTopicCapitalizedROM getProperties];
    
    NSArray * responseArray = [responseProperties allObjects];
    NSArray * capitalizedArray = [capitalizedProperties allObjects];
    
    responseArray = [responseArray sortedArrayUsingSelector:@selector(compare:)];
    capitalizedArray = [capitalizedArray sortedArrayUsingSelector:@selector(compare:)];
    
    for (NSUInteger i=0; i<responseArray.count; i++ ) {
        NSString * keyOne = [responseArray objectAtIndex:i];
        NSString * keyTwo = [capitalizedArray objectAtIndex:i];
        XCTAssertEqualObjects([[responseObject valueForKey:keyOne] class], [[capitalizedObject valueForKey:keyTwo] class], @"Properties mismatch: %@ - %@ \"%s\"", [[responseObject valueForKey:keyOne] class], [[capitalizedObject valueForKey:keyTwo] class] ,__PRETTY_FUNCTION__);
    }
}

- (void)testArrayOfObjects
{
    NSString *filePath = [[NSBundle mainBundle] pathForResource:@"TopicResponseArray" ofType:@"json"];
    NSString * jsonString = [[NSString alloc] initWithContentsOfFile:filePath encoding:NSUTF8StringEncoding error:nil];
    
    NSError * e;
    
    NSArray * response = [NSJSONSerialization JSONObjectWithData:[jsonString dataUsingEncoding:NSUTF8StringEncoding] options:kNilOptions error:&e];
    
    NSArray * responseArray = [LXNResponseParser parseResponse:response usingClass:[LXNTopicROM class]];

    XCTAssertEqual(response.count, responseArray.count, @"Items count mismatch: %ld %ld \"%s\"", (unsigned long)response.count, (unsigned long)responseArray.count, __PRETTY_FUNCTION__);
}

- (void)testArrayOfCapitalizedObjects
{
    NSString *filePath = [[NSBundle mainBundle] pathForResource:@"TopicResponseArray" ofType:@"json"];
    NSString * jsonString = [[NSString alloc] initWithContentsOfFile:filePath encoding:NSUTF8StringEncoding error:nil];
    
    NSError * e;
    
    NSArray * response = [NSJSONSerialization JSONObjectWithData:[jsonString dataUsingEncoding:NSUTF8StringEncoding] options:kNilOptions error:&e];
    
    NSArray * responseArray = [LXNResponseParser parseResponse:response usingClass:[LXNTopicCapitalizedROM class]];
    
    XCTAssertEqual(response.count, responseArray.count, @"Items count mismatch: %ld %ld \"%s\"", (unsigned long)response.count, (unsigned long)responseArray.count, __PRETTY_FUNCTION__);
}


- (void)testParseFromNSString
{
    NSString *filePath = [[NSBundle mainBundle] pathForResource:@"SingleTopic" ofType:@"json"];
    NSString * jsonString = [[NSString alloc] initWithContentsOfFile:filePath encoding:NSUTF8StringEncoding error:nil];
    
    id responseObject = [LXNResponseParser parseResponse:jsonString usingClass:[LXNTopicROM class]];
    
    XCTAssertTrue([responseObject isKindOfClass:[LXNTopicROM class]], @"Object is not kind of class %@ - %@ \"%s\"", [LXNTopicROM class], [responseObject class], __PRETTY_FUNCTION__);
}

- (void)testParseFromNSData
{
    NSString *filePath = [[NSBundle mainBundle] pathForResource:@"SingleTopic" ofType:@"json"];
    NSString * jsonString = [[NSString alloc] initWithContentsOfFile:filePath encoding:NSUTF8StringEncoding error:nil];
    
    NSData * jsonData = [jsonString dataUsingEncoding:NSUTF8StringEncoding];
    id responseObject = [LXNResponseParser parseResponse:jsonData usingClass:[LXNTopicROM class]];
    
    XCTAssertTrue([responseObject isKindOfClass:[LXNTopicROM class]], @"Object is not kind of class %@ - %@ \"%s\"", [LXNTopicROM class], [responseObject class], __PRETTY_FUNCTION__);
}

- (void)testParseObjectWithDateFormatter
{
    NSString *filePath = [[NSBundle mainBundle] pathForResource:@"SingleTopic" ofType:@"json"];
    NSString * jsonString = [[NSString alloc] initWithContentsOfFile:filePath encoding:NSUTF8StringEncoding error:nil];
    
    NSError * e;
    
    id response = [NSJSONSerialization JSONObjectWithData:[jsonString dataUsingEncoding:NSUTF8StringEncoding] options:kNilOptions error:&e];
    
    LXNTopicROM * responseObject = [LXNResponseParser parseResponse:response usingClass:[LXNTopicROM class]];
    
    XCTAssertTrue([responseObject.created_at isKindOfClass:[NSDate class]], @"Object is not kind of class %@ - %@ \"%s\"", [NSDate class], [responseObject class] ,__PRETTY_FUNCTION__);
}

- (void)testNilResponse
{
    id responseObject = [LXNResponseParser parseResponse:nil usingClass:[LXNTopicROM class]];
    
    XCTAssertNil(responseObject, @"Object is not not nill \"%s\"", __PRETTY_FUNCTION__);
}

- (void)testEmptyStringResponse
{
    id responseObject = [LXNResponseParser parseResponse:@"" usingClass:[LXNTopicROM class]];
    
    XCTAssertNil(responseObject, @"Object is not not nill \"%s\"", __PRETTY_FUNCTION__);
}

- (void)testCustomKeyMapping
{
    NSString *filePath = [[NSBundle mainBundle] pathForResource:@"SingleTopic" ofType:@"json"];
    NSString * jsonString = [[NSString alloc] initWithContentsOfFile:filePath encoding:NSUTF8StringEncoding error:nil];
    
    id responseObject = [LXNResponseParser parseResponse:jsonString usingClass:[LXNTopicTypoROM class]];
    
    XCTAssertNotNil([responseObject nameuser], @"Property is nil %@ \"%s\"", @"username", __PRETTY_FUNCTION__);
}

- (void)testCustomProppertyMapping
{
    NSString *filePath = [[NSBundle mainBundle] pathForResource:@"SingleTopic" ofType:@"json"];
    NSString * jsonString = [[NSString alloc] initWithContentsOfFile:filePath encoding:NSUTF8StringEncoding error:nil];
    
    id responseObject = [LXNResponseParser parseResponse:jsonString usingClass:[LXNTopicAdditionalPropertiesROM class]];
    
    XCTAssertTrue([responseObject isImage], @"Property is false %@ \"%s\"", @"isImage", __PRETTY_FUNCTION__);
}



@end
