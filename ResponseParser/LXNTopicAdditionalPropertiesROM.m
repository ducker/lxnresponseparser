//
//  LXNTopicAdditionalPropertiesROM.m
//  ResponseParser
//
//  Created by Leszek Kaczor on 06/02/15.
//  Copyright (c) 2015 Untitled Kingdom. All rights reserved.
//

#import "LXNTopicAdditionalPropertiesROM.h"

@implementation LXNTopicAdditionalPropertiesROM

- (BOOL)customMappingForPropertyName:(NSString *)propertyName value:(NSObject *)value
{
    if ([propertyName isEqualToString:@"image"])
    {
        self.image = (NSString *)value;
        if (value) self.isImage = YES;
        else self.isImage = NO;
        return YES;
    }
    
    return [super customMappingForPropertyName:propertyName value:value];
}

@end
