//
//  UKTopicROM.h
//  ResponseParser
//
//  Created by Leszek Kaczor on 04.04.2014.
//  Copyright (c) 2014 Untitled Kingdom. All rights reserved.
//

#import "LXNAbstractROM.h"

@interface LXNTopicROM : LXNAbstractROM

@property (nonatomic        ) BOOL     active;
@property (nonatomic, strong) NSNumber * comments_count;
@property (nonatomic, strong) NSNumber * points;
@property (nonatomic, strong) NSArray  * tags;
@property (nonatomic, strong) NSDate   * updated_at;
@property (nonatomic, strong) NSNumber * views_count;


@property (nonatomic, strong) NSDate   * created_at;
@property (nonatomic, strong) NSString * discussion;
@property (nonatomic, strong) NSString * image;
@property (nonatomic, strong) NSNumber * old_id;
@property (nonatomic, strong) NSString * permalink;
@property (nonatomic        ) BOOL     featured;
@property (nonatomic, strong) NSString * username;
@property (nonatomic, strong) NSString * category_title;
@property (nonatomic, strong) NSNumber * ticckles_count;
@property (nonatomic, strong) NSNumber * replies_count;
@property (nonatomic, strong) NSString * user_presence;
@property (nonatomic, strong) NSNumber * user_old_id;
@property (nonatomic, strong) NSString * user_avatar_url;

@end
