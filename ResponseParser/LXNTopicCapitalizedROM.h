//
//  UKTopicCapitalizedROM.h
//  ResponseParser
//
//  Created by Leszek Kaczor on 07.04.2014.
//  Copyright (c) 2014 Untitled Kingdom. All rights reserved.
//

#import "LXNAbstractROM.h"

@interface LXNTopicCapitalizedROM : LXNAbstractROM

@property (nonatomic        ) BOOL     active;
@property (nonatomic, strong) NSNumber * commentsCount;
@property (nonatomic, strong) NSNumber * points;
@property (nonatomic, strong) NSArray  * tags;
@property (nonatomic, strong) NSDate   * updatedAt;
@property (nonatomic, strong) NSNumber * viewsCount;


@property (nonatomic, strong) NSDate   * createdAt;
@property (nonatomic, strong) NSString * discussion;
@property (nonatomic, strong) NSString * image;
@property (nonatomic, strong) NSNumber * oldId;
@property (nonatomic, strong) NSString * permalink;
@property (nonatomic        ) BOOL     featured;
@property (nonatomic, strong) NSString * username;
@property (nonatomic, strong) NSString * categoryTitle;
@property (nonatomic, strong) NSNumber * ticcklesCount;
@property (nonatomic, strong) NSNumber * repliesCount;
@property (nonatomic, strong) NSString * userPresence;
@property (nonatomic, strong) NSNumber * userOldId;
@property (nonatomic, strong) NSString * userAvatarUrl;

@end
