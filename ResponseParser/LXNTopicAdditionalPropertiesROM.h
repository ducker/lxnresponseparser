//
//  LXNTopicAdditionalPropertiesROM.h
//  ResponseParser
//
//  Created by Leszek Kaczor on 06/02/15.
//  Copyright (c) 2015 Untitled Kingdom. All rights reserved.
//

#import "LXNTopicROM.h"

@interface LXNTopicAdditionalPropertiesROM : LXNTopicROM

@property (nonatomic) BOOL isImage;

@end
