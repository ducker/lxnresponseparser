//
//  UKViewController.m
//  ResponseParser
//
//  Created by Leszek Kaczor on 04.04.2014.
//  Copyright (c) 2014 Untitled Kingdom. All rights reserved.
//

#import "LXNViewController.h"
#import "LXNResponseParser.h"
#import "LXNTopicROM.h"

@interface LXNViewController ()

@end

@implementation LXNViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
