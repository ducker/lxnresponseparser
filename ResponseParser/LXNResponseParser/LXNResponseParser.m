//
//  UKResponseParser.m
//  Untitled Kingdom
//
//  Created by Leszek Kaczor on 10/14/13.
//  Copyright (c) 2013 Untitled Kingdom. All rights reserved.
//

#import "LXNResponseParser.h"
#import "LXNAbstractROM.h"
#import <objc/runtime.h>

@implementation LXNResponseParser

static BOOL logTime;
static NSDateFormatter * dateFormatter;

#pragma mark - Public methods
+ (id)parseResponse:(id)response usingClass:(Class)class
{
    if (!response) return nil;
    if ([class isKindOfClass:[NSNull class]]) return nil;
    NSDate * date = [NSDate date];
    if ([response isKindOfClass:[NSString class]])
        response = [response dataUsingEncoding:NSUTF8StringEncoding];
    if ([response isKindOfClass:[NSData class]])
        response = [NSJSONSerialization JSONObjectWithData:response options:kNilOptions error:nil];
    id result = [self parseResponseObjects:response usingClass:class];
    if (logTime)
        NSLog(@"Parse class object: %@ time taken: %f", class, [[NSDate date] timeIntervalSinceDate:date]);
    return result;
}

+ (void)logTime:(BOOL)log
{
    logTime = log;
}

+ (void)dateFormatter:(NSDateFormatter *)formatter
{
    dateFormatter = formatter;
}

#pragma mark - Private methods
+ (id)parseResponseObjects:(id)response usingClass:(Class)class
{
    if (![class isSubclassOfClass:[LXNAbstractROM class]]) return response;
        
    if ([response isKindOfClass:[NSArray class]])
    {
        NSMutableArray *blockResponseArray = [NSMutableArray array];
        for (id obj in response)
        {
            id newObject = [self createObjectOfClass:class fromDictionary:obj];
            if (newObject)
                [blockResponseArray addObject:newObject];
        }
        response = [blockResponseArray copy];
    } else {
        response = [self createObjectOfClass:class fromDictionary:response];
    }

    return response;
}

+ (id)createObjectOfClass:(Class)class fromDictionary:(NSDictionary*)dictionary
{
    id newObject = nil;
    NSDictionary * dictionaryObject = dictionary;
    NSString * rootKey = [class getRootObjectKey];
    if (rootKey && [dictionaryObject objectForKey:rootKey])
        dictionaryObject = [dictionaryObject objectForKey:rootKey];
    
    for (NSString * propertyName in [class getProperties])
    {
        Class customClass = [class getCustomClassForPropertyName:propertyName];
        id value = [self getValueForPropertyNamed:propertyName fromDictionary:dictionaryObject forClass:class];
        value = [self parseResponseObjects:value usingClass:customClass];
        NSString * propertyTypeString = [self typeOfPropertyNamed:propertyName forClass:class];
        if (value && ![value isKindOfClass:[NSNull class]])
        {
            if (!newObject) newObject = [[class alloc] init];
            if ([(LXNAbstractROM *)newObject customMappingForPropertyName:propertyName value:value]) {
            } else if (dateFormatter && [propertyTypeString isEqualToString:@"T@\"NSDate\""])
                [newObject setValue:[dateFormatter dateFromString:value] forKey:propertyName];
            else
                [newObject setValue:value forKey:propertyName];
        }
    }
    return newObject;
}

+ (id)getValueForPropertyNamed:(NSString*)name fromDictionary:(NSDictionary*)dict forClass:(Class)parseClass
{
    NSString * key = [parseClass customKeyMappingDictionary][name];
    if (key)
        return dict[key];
    
    id result = dict[name];
    if (!result)
    {
        static NSRegularExpression * regex;
        if (!regex)
            regex = [NSRegularExpression regularExpressionWithPattern:@"([a-z])([A-Z]+)" options:kNilOptions error:nil];
        NSString *replaced = [regex stringByReplacingMatchesInString:name
                                                             options:kNilOptions
                                                               range:NSMakeRange(0, name.length)
                                                        withTemplate:@"$1_$2"];
        replaced = [replaced lowercaseString];
        result = dict[replaced];
    }
    return result;
}

+ (NSString *)typeOfPropertyNamed:(NSString *)name forClass:(Class)class
{
	objc_property_t property = class_getProperty(class, [name UTF8String]);
	if (property == NULL) return nil;
    
    const char * attrs = property_getAttributes(property);
	if (attrs == NULL) return nil;
    
	static char type[256];
	const char * end = strchr(attrs, ',');
	if (end == NULL) return nil;
    
	int len = (int)(end - attrs);
	memcpy(type, attrs, len);
	type[len] = '\0';
    
	return [NSString stringWithCString:type encoding:NSUTF8StringEncoding];
}

@end
