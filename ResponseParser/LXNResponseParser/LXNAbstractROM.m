//
//  UKAbstractROM.m
//  Untitled Kingdom
//
//  Created by Leszek Kaczor on 4/4/14.
//  Copyright (c) 2014 Untitled Kingdom. All rights reserved.
//

#import "LXNAbstractROM.h"
#import <objc/runtime.h>

@implementation LXNAbstractROM

+(NSSet *)getProperties
{
    id currentClass = [self class];
    NSString *propertyName;
    NSMutableSet *propertySet = [NSMutableSet set];
    unsigned int outCount, i;
    objc_property_t *properties = class_copyPropertyList(currentClass, &outCount);
    for (i = 0; i < outCount; i++) {
    	objc_property_t property = properties[i];
        const char *attributes = property_getAttributes(property);
        NSString *encoding = [NSString stringWithCString:attributes encoding:NSUTF8StringEncoding];
        if ([[encoding componentsSeparatedByString:@","] containsObject:@"R"])
            continue;
    	propertyName = [NSString stringWithCString:property_getName(property) encoding:NSUTF8StringEncoding];
    	[propertySet addObject:propertyName];
    }
    Class superClass = class_getSuperclass(currentClass);
    if ([superClass isSubclassOfClass:[LXNAbstractROM class]])
        [propertySet addObjectsFromArray:[[superClass getProperties] allObjects]];
    return [propertySet copy];
}

+ (Class)getCustomClassForPropertyName:(NSString*)property
{
    return [NSNull class];
}

+ (NSString*)getRootObjectKey
{
    return nil;
}

- (BOOL)customMappingForPropertyName:(NSString *)propertyName value:(NSObject *)value
{
    return NO;
}

+ (NSDictionary *)customKeyMappingDictionary
{
    return @{@"objectId" : @"id", @"objectDescription" : @"description"};
}

@end
