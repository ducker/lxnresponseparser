//
//  UKAbstractROM.h
//  Untitled Kingdom
//
//  Created by Leszek Kaczor on 4/4/14.
//  Copyright (c) 2014 Untitled Kingdom. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface LXNAbstractROM : NSObject

/**
 *  Function returns set of properties that are not marked as readonly.
 *
 *  @return set of properties that are not marked as readonly
 */
+ (NSSet *)getProperties;

/**
 *  Function returns custom class for given property name. If property is object of class from
 *  Foundation Framework function should return it's superclass value.
 *
 *  @param property the name of property
 *
 *  @return the custom class for property
 */
+ (Class)getCustomClassForPropertyName:(NSString*)property;

/**
 *  Function defines root object of json object.
 *
 *  @return the name of root object of json object
 */
+ (NSString*)getRootObjectKey;

/**
 *  Function allows to set custom mapping of property.
 *
 *  @param propertyName the name of property
 *  @param value        the value from parsing JSON
 *
 *  @return BOOL value which determines if used custom mapping for property
 */
- (BOOL)customMappingForPropertyName:(NSString *)propertyName value:(NSObject *)value;

/**
 *  Function defines custom key mapping
 *
 *  @return the mapping dictionary. Dictionary key is property name and value is JSON key.
 */
+ (NSDictionary *)customKeyMappingDictionary;
@end
