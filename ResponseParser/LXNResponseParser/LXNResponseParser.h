//
//  UKResponseParser.h
//  Untitled Kingdom
//
//  Created by Leszek Kaczor on 10/14/13.
//  Copyright (c) 2013 Untitled Kingdom. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface LXNResponseParser : NSObject

/**
 *  Function allows to enable or disable logging time of parsing response
 *
 *  @param log BOOL value
 */
+ (void)logTime:(BOOL)log;

/**
 *  Setter for date formatter used to parse dates
 *
 *  @param formatter NSDateFormatter user to parse json objects dates
 */
+ (void)dateFormatter:(NSDateFormatter*)formatter;

/**
 *  Function parse response to object of defined class.
 *
 *  @param response response to parse. It can be NSString, NSData, NSArray or NSDictionary
 *  @param class    subclass of UKAbstractROM class
 *
 *  @return parsed response
 */
+ (id)parseResponse:(id)response usingClass:(Class)class;

@end
