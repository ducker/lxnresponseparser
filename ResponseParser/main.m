//
//  main.m
//  ResponseParser
//
//  Created by Leszek Kaczor on 04.04.2014.
//  Copyright (c) 2014 Untitled Kingdom. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "LXNAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([LXNAppDelegate class]));
    }
}
