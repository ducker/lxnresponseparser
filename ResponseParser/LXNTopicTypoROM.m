//
//  LXNTopicTypoROM.m
//  ResponseParser
//
//  Created by Leszek Kaczor on 06/02/15.
//  Copyright (c) 2015 Untitled Kingdom. All rights reserved.
//

#import "LXNTopicTypoROM.h"

@implementation LXNTopicTypoROM

+ (NSDictionary *)customKeyMappingDictionary
{
    return @{@"nameuser" : @"username"};
}

@end
