## LXNResponseParser

This is the repository for LXNResponseParser.

## Installation

Add following line to your Podfile

```
pod 'LXNResponseParser', :git => "https://bitbucket.org/ducker/lxnresponseparser.git"
```

If no cocoapods installed go to http://cocoapods.org for further informations.

## Usage

### Basic usage

Your model class must be a LXNAbstractROM subclass. If your JSON looks like:
```json
{
   "string_property":"some text",
   "number_property":100,
   "array_property":[
      "text1",
      "text2",
      "text3"
   ],
   "some_date":"2015-01-6T11:15:52Z",
   "bool_flag":true
}
```

Your model class should be (you can use either property_name and propertyName style)

```objective-c
@interface MyModelROM : LXNAbstractROM

@property (nonatomic, strong) NSString * stringProperty;
@property (nonatomic, strong) NSNumber * numberProperty;
@property (nonatomic, strong) NSArray  * arrayProperty;
@property (nonatomic, strong) NSDate   * someDate;
@property (nonatomic        ) BOOL     boolFlag;

@end
```

It's all you have to do with your model. If you use NSDate you can specify date formatter for LXNResponseParser

```objective-c
NSDateFormatter * formatter = [NSDateFormatter new];
[formatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss'Z'"];
[LXNResponseParser dateFormatter:formatter];
```

And that's it. You can parse JSON using LXNResponseParser. JSON can be NSDictionary, NSArray, NSString or NSDate.

```objective-c
MyModelROM * responseObject = [LXNResponseParser parseResponse:JSON usingClass:[MyModelROM class]];
```

If your response returns array of objects the only thing you have to change is responseObject type

```objective-c
NSArray * responseObjectArray = [LXNResponseParser parseResponse:JSON usingClass:[MyModelROM class]];
```


### Advanced usage

You can specify which properties should be parsed by overriding getProperties method.

```objective-c
+ (NSSet *)getProperties
{
    NSMutableSet * properties = [[super getProperties] mutableCopy];
    [properties removeObject:@"stringProperty"];
    return [properties copy];
}
```

If your model class contains another model as property you have to specify it in getCustomClassForPropertyName method. If you don't do it it will pass NSDictionary object (or NSDictionary objects array).

```objective-c
@interface MyAnotherModelROM : LXNAbstractROM

@property (nonatomic, strong) MyModelROM  * myModelObject;
@property (nonatomic, strong) NSArray   * myModelObjectArray;

@end

@implementation MyAnotherModelROM

+ (Class)getCustomClassForPropertyName:(NSString *)property
{
    if ([property isEqualToString:@"myModelObject"]) return [MyModelROM class];
    if ([property isEqualToString:@"myModelObjectArray"]) return [MyModelROM class];
    return [super getCustomClassForPropertyName:property];
}

@end

```

Method getRootObjectKey can be used for example if JSON has root key before objects and we want to have just object without it.

```json
[
   {
      "object":{
         "string_property":"some text",
         "number_property":100,
         "array_property":[
            "text1",
            "text2",
            "text3"
         ],
         "some_date":"2015-01-6T11:15:52Z",
         "bool_flag":true
      }
   },
   {
      "object":{
         "string_property":"some text",
         "number_property":100,
         "array_property":[
            "text1",
            "text2",
            "text3"
         ],
         "some_date":"2015-01-6T11:15:52Z",
         "bool_flag":true
      }
   },
   {
      "object":{
         "string_property":"some text",
         "number_property":100,
         "array_property":[
            "text1",
            "text2",
            "text3"
         ],
         "some_date":"2015-01-6T11:15:52Z",
         "bool_flag":true
      }
   }
]
```

You should always use super method if not returning custom value, because you can inherits from your models.

```objective-c
@interface ExtendedModelROM : MyModelROM

@property (nonatomic, strong) NSString * anotherStringProperty;
@property (nonatomic, strong) NSNumber * anotherNumberProperty;

@end

```

### Custom mapping

If you want to use different key that in your JSON object you should specify it in customKeyMappingDictionary method. Default mapping prevents from using id and description fields of NSObject.

```objective-c
+ (NSDictionary *)customKeyMappingDictionary
{
    return @{@"objectId" : @"id", @"objectDescription" : @"description"};
}
```

Custom property mapping is provided by customMappingForPropertyName:value: method.

```objective-c
- (BOOL)customMappingForPropertyName:(NSString *)propertyName value:(NSObject *)value
{
    if ([propertyName isEqualToString:@"stringProperty"])
    {
        self.image = [(NSString *)value capitalizedString];
        return YES;
    }
    
    return [super customMappingForPropertyName:propertyName value:value];
}
```


## Updating

### Notes